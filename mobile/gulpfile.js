var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var uglify      = require('gulp-uglify');
var pump        = require('pump');
var rev         = require('gulp-rev');
var inject      = require('gulp-inject');
var clean       = require('gulp-clean');


gulp.task('serve', ['inject'], function() {

    browserSync.init({
        server: {
        	baseDir: './'
        }
    });

    gulp.watch("sass/*.scss", ['inject']);
    gulp.watch(["*.html", '*.js'], ['inject']).on('change', browserSync.reload);
});


gulp.task('sass', function() {
    return gulp.src("sass/*.scss")
        .pipe(sass())
        .pipe(rev())
        .pipe(gulp.dest("./dist/css"))
        .pipe(browserSync.stream());
});

gulp.task('compress', function(cb) {
    pump([
            gulp.src(['scripts.js', 'mobile-core-template.js']),
            uglify(),
            rev(),
            gulp.dest('dist/js')
        ],
        cb
        );
});

gulp.task('inject', ['clean', 'sass', 'compress'], function() {
    var target = gulp.src('./index.html');
    var sources = gulp.src(['./dist/js/scripts-*.js', './dist/js/mobile-core-template-*.js', './dist/css/style-*.css' ]);
    return target.pipe(inject(sources))
        .pipe(gulp.dest('./'));
});

gulp.task('clean',function(){
    return gulp.src(['./dist/js/*', './dist/css/*'], {read: false})
        .pipe(clean());
});