var connectViewTimeline;
var navigationViewTimeline;
var couponViewTimeline;
var loaderViewTimeline;
var photoViewTimeline;
var couponSubmitedViewTimeline;
var connectionErrorTimeline;

var connectButton = document.getElementById('connect-button');
var getCouponButton = document.getElementById('coupon-button');
var submitCouponButton =document.getElementById('submit-coupon-button');



window.addEventListener('load', function(){
	
	createConnectViewTimeline();
	createNavigationViewTimeline();
	createCouponViewTimeline();
	createLoaderViewTimeline();
	createPhotoViewTimeline();
	createCouponSubmitedViewTimeline();
	createConnectionErrorTimeline();

	// if (connectButton != null) {
	// 	connectButton.addEventListener('touchstart',function(){
	// 		goToNavigationView();
	// 	})
	// }
	// if (getCouponButton != null) {
	// 	getCouponButton.addEventListener('touchstart',function(){
	// 		goToCouponView();
	// 	})
	// }
	// if (submitCouponButton != null) {
	// 	submitCouponButton.addEventListener('touchstart',function(){
	// 		goToNavigationView();
	// 	})
	// }
})

function createConnectViewTimeline() {
	connectViewTimeline = new TimelineLite();
	connectViewTimeline.pause();
	connectViewTimeline.from('#connect-button', 0.5, {marginLeft: "-200vw"});
	connectViewTimeline.from('.connect-view input', 0.5, {marginRight: "-200vw"},0);
	connectViewTimeline.from('.connect-view input, #connect-button', 0.01, {opacity: "0"},0);
	connectViewTimeline.to('.connect-view', 0.01, {zIndex: "101"},0);
}

function resetConnectView() {
	$('.connect-view, .connect-view input, .connect-view button').removeAttr('style');
	createConnectViewTimeline();
	showConnectView();
}

function createNavigationViewTimeline() {
	navigationViewTimeline = new TimelineLite();
	navigationViewTimeline.pause();
	navigationViewTimeline.from('.photo-button', 0.5, {marginLeft: "-200vw"},0);
	navigationViewTimeline.from('.coupon-button', 0.5, {marginRight: "-200vw"}, 0);
	navigationViewTimeline.from('.navigation.left', 0.5, {marginLeft: "-100vw"},0);
	navigationViewTimeline.from('.navigation.right', 0.5, {marginRight:"-100vw"},0);
	navigationViewTimeline.from('.control-view',0.01, {opacity:"0"},0);
	navigationViewTimeline.to('.control-view',0.01, {zIndex:"101"},0);
}
	
function createCouponViewTimeline() {
	couponViewTimeline = new TimelineLite();
	couponViewTimeline.pause();
	couponViewTimeline.from('.coupon-view', 0.5, {top: "-110vh"},0);
	couponViewTimeline.from('.coupon-view', 0.01, {opacity:"0"},0);
	couponViewTimeline.to('.coupon-view', 0.01, {zIndex:"101"},0);
}
function resetCouponView() {
	$('.coupon-view, .coupon-view input, .coupon-view button').removeAttr('style');
	createCouponViewTimeline();
	showCouponView();
}
	
function createLoaderViewTimeline() {
	loaderViewTimeline = new TimelineLite();
	loaderViewTimeline.pause();
	loaderViewTimeline.to('.loader-view', 0.5, {opacity:0}, 0);
	loaderViewTimeline.to('.loader-view', 0.01, {zIndex:"1"}, 0.5);
}

function createPhotoViewTimeline() {
	photoViewTimeline = new TimelineLite();
	photoViewTimeline.pause();
	photoViewTimeline.from('.photo-view',0.5, {opacity:0}, 0);
	photoViewTimeline.to('.photo-view', 0.01, {zIndex:"101"},0);
}

function createCouponSubmitedViewTimeline() {
	couponSubmitedViewTimeline = new TimelineLite();
	couponSubmitedViewTimeline.pause();
	couponSubmitedViewTimeline.from('.coupon-submited-view', 0.5, {opacity:0},0);
	couponSubmitedViewTimeline.to('.coupon-submited-view', 0.01, {zIndex:"101"},0);
}
	
function createConnectionErrorTimeline() {
	connectionErrorTimeline = new TimelineLite();
	connectionErrorTimeline.pause();
	connectionErrorTimeline.from('.connection-error-view', 0.5, {opacity:0},0);
	connectionErrorTimeline.to('.connection-error-view', 0.01, {zIndex:"101"},0);
}

function showConnectView() {
	connectViewTimeline.play();
}
function hideConnectView() {
	connectViewTimeline.reverse();
}
function showNavigationView(){
	navigationViewTimeline.play();
}
function hideNavigationView() {
	navigationViewTimeline.reverse();
}
function showCouponView() {
	couponViewTimeline.play();
}
function hideCouponView() {
	couponViewTimeline.reverse();
}
function hideLoaderView() {
	loaderViewTimeline.play();
}
function showLoaderView() {
	loaderViewTimeline.reverse();
}
function showPhotoView() {
	photoViewTimeline.play();
}
function hidePhotoView() {
	photoViewTimeline.reverse();
}
function showCouponSubmitedView() {
	couponSubmitedViewTimeline.play();
}
function hideCouponSubmitedView() {
	couponSubmitedViewTimeline.reverse();
}
function showConnectionErrorView() {
	connectionErrorTimeline.play();
}
function hideConnectionErrorView() {
	connectionErrorTimeline.reverse();
}

function goToPhotoView() {
	hideNavigationView();
	hideCouponView();
	hideConnectView();
	hideLoaderView();
	hideConnectionErrorView();
	hideCouponSubmitedView();
	setTimeout(function(){
		showPhotoView();
	},500);
}

function goToLoaderView() {
	hideNavigationView();
	hideCouponView();
	hideConnectView();
	hidePhotoView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	// setTimeout(function(){
	// 	showLoaderView();
	// },500);
	showLoaderView();
}

function goToConnectView() {
	hideNavigationView();
	hideCouponView();
	hideLoaderView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hidePhotoView();
	setTimeout(function(){
		showConnectView();
	},500);
}

function goToNavigationView() {
	hideConnectView();
	hideCouponView();
	hidePhotoView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hideLoaderView();
	setTimeout(function(){
		showNavigationView();
	}, 500);
}

function goToCouponView() {
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hideCouponSubmitedView();
	hideConnectionErrorView();
	hidePhotoView();
	setTimeout(function(){
		showCouponView();
	}, 500);
}
function goToCouponSubmitedView() {
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hidePhotoView();
	hideConnectionErrorView();
	hideNavigationView();
	setTimeout(function(){
		showCouponSubmitedView();
	}, 500);

}
function goToConnectionErrorView() {
	hideConnectView();
	hideCouponView();
	hideLoaderView();
	hidePhotoView();
	hideNavigationView();
	hideCouponSubmitedView();
	setTimeout(function(){
		showConnectionErrorView();
	}, 500);
}
